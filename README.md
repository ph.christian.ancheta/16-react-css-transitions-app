# CSS Transitions - React App
A simple React App that allows users to add items, remove items, and toggle sections in the application with the main focus being frontend transitions with CSS and animations.

## Setup

```
npm install
npm run start
```
